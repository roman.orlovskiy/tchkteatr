import React, {PureComponent} from 'react';
import Tickets from 'pages/Tickets/Tickets';

class Root extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  render() {
    return (
      <Tickets />
    );
  }
}

export default Root;
