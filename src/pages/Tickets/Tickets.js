import React, {Component} from 'react';
import Rectangle from 'blocks.simple/Rectangle';

import cls from 'classnames';
import styles from './Tickets.module.scss';
import qs from 'query-string';
import md5 from 'md5';
import Icon from 'blocks.simple/Icon';
import Bb from 'pages/Tickets/bb';
import Mayak from 'pages/Tickets/mayak';

class Tickets extends Component {
  constructor(props) {
    super(props);

    this.events = [
      {
        id: '1268535',
        date: '23 мар',
        time: '19:00',
        name: 'Бешеная балерина',
        price: 600
      }
    ];
    this.pagesConfig = {
      bb: {
        title: 'Спектакль «Бешеная Балерина»',
        header: 'bb.jpg',
        button: 'Смотреть спектакль «Бешеная Балерина»',
        theme: 'info',
        descrComponent: Bb,
        hash: '3101b6989fabc6a527ef911732fbd9b9',
        linkId: 'ipYYt5fL3jw'
      },
      mayak: {
        title: 'Спектакль «Маяковский в жёлтом»',
        header: 'mayak.jpg',
        button: 'Смотреть спектакль «Маяковский в жёлтом»',
        theme: 'warning',
        descrComponent: Mayak,
        hash: '121c766978ecec7f2618d483fd037943',
        linkId: 'gK22CFJ5dWM'
      }
    }

    this.state = {
      selectedEvent: this.events[0],
      viewPass: '',
      selectedPage: 'bb'
    };
  }

  onChangeEvent = ({target: {value}}) => {
    const selectedEvent = this.events.find(({id}) => id === value);
    this.setState({selectedEvent});
  };

  onChangeViewPass = ({target: {value: viewPass}}) => {
    this.setState({viewPass});
  };

  getLink = () => {
    const {selectedEvent} = this.state;
    const query = qs.parse(window.location.search);
    let link = `https://tchk-teatr.timepad.ru/event/${selectedEvent.id}/`;

    if (query.utm_campaign && query.utm_source) {
      link += `?${qs.stringify(query)}`;
    }

    link += '#register';

    return link;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { viewPass, selectedPage } = this.state;
    const pageConfig = this.pagesConfig[selectedPage];
    const { hash } = pageConfig;

    if (md5(viewPass) === hash) {
      this.showView();
    } else {
      alert('Неправильный пароль. Для просмотра позвоните +7(910)875-24-89');
    }
  }

  showView = () => {
    this.setState({showView1: true}, () => {
      setTimeout(() => {
        this.setState({showView2: true});
      }, 50);
      setTimeout(() => {
        document.body.style.overflow = 'hidden';
      }, 600);
    });
  }

  hideView = () => {
    this.setState({showView2: false}, () => {
      setTimeout(() => {
        this.setState({showView1: false});
        document.body.style.overflow = '';
      }, 1000);
    });
  }

  render() {
    const { viewPass, showView1, showView2, selectedPage } = this.state;
    const pageConfig = this.pagesConfig[selectedPage];
    const { title, header, button, theme, descrComponent, linkId } = pageConfig;
    const Descr = descrComponent;

    return (
      <div className={cls('container', styles.wrapper)}>
        <div className="text-center">
          <div className="font-size-md-30 font-size-26 mb-2"><span>ТЧК</span> Театр</div>
          <div className="d-flex flex-column align-items-center">
            <div
              className={cls('mb-3 btn btn-info', {'btn-lg': (selectedPage === 'bb')})}
              onClick={() => this.setState({selectedPage: 'bb'})}
            >
              «Бешеная Балерина»
            </div>
            <div
              className={cls('mb-4 btn btn-warning', {'btn-lg': (selectedPage === 'mayak')})}
              onClick={() => this.setState({selectedPage: 'mayak'})}
            >
              «Маяковский в жёлтом»
            </div>
          </div>
          <div className="font-size-md-22 font-size-18 mb-2">
            Для просмотра пишите <a target="_blank" rel="noopener noreferrer" href="https://vk.com/kibardin_konstantin" className="text-info font-weight-bold">Константину&nbsp;Кибардину</a>
          </div>
          <div className="font-size-md-22 font-size-18 mb-2">
            Или звоните <a href="tel:+7(910)875-24-89" className="text-info font-weight-bold">+7&nbsp;(910)&nbsp;875-24-89</a>
          </div>
        </div>
        <div className={styles.header}>
          <Rectangle
            width={15.9}
            height={4}
            src={`/images/${header}`}
            className="shadow"
          />
        </div>
        <div className="mt-3 mb-3">
          <div className="font-size-md-20 font-size-18 pl-3 mb-3 text-center">
            <b>Закрытый показ</b>
          </div>
          <div className="font-size-md-18 font-size-16 text-left pl-3 pr-3">
            <Descr />
            <br /><br />
            Когда всё вернётся, ждем вас в нашем <a target="_blank" rel="noopener noreferrer" href="https://vk.com/feed?c%5Bq%5D=%23%F2%F7%EA%F2%E5%E0%F2%F0&section=search" className="text-info font-weight-bold">#тчктеатр</a>.
            <br />
            Получить доступ к просмотру можно по телефону: <a href="tel:+7(910)875-24-89" className="text-info font-weight-bold">+7&nbsp;(910)&nbsp;875-24-89</a> (Константин).
          </div>
        </div>

        <form
          className={cls(styles.buyTicket, 'shadow-lg')}
          onSubmit={this.handleSubmit}
        >
          <div className="mb-3">
            <input
              className={styles.input}
              value={viewPass}
              onChange={this.onChangeViewPass}
              placeholder="Введите пароль для просмотра"
            />
          </div>
          <div className={styles.ticketButtonLg}>
            <button
              type="submit"
              className={cls(`shadow-lg btn btn-${theme} btn-block btn-lg`)}
            >
              {button}
            </button>
          </div>
          <div className={styles.ticketButtonSm}>
            <button
              type="submit"
              className={cls(`shadow-lg btn btn-${theme} btn-block btn`)}
            >
              {button}
            </button>
          </div>
        </form>

        {showView1 &&
        <div className={cls(styles.view, 'shadow-lg', {[styles.showView]: showView2})}>
          <iframe
            title={title}
            width="100%"
            height="100%"
            src={`https://www.youtube.com/embed/${linkId}`}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          >
          </iframe>
          <div
            title={'Закрыть просмотр'}
            onClick={this.hideView}
            className={cls(styles.icon, 'shadow-lg')}
          >
            <Icon
              type={'close'}
              size={'30'}
            />
          </div>
        </div>
        }
      </div>
    );
  }
}

export default Tickets;
