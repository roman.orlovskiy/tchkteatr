import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import 'normalize.css';
import * as serviceWorker from './serviceWorker';

const Root = require('blocks.app/Root').default;
const rootElement = document.getElementById('root');
ReactDOM.render(<Root />, rootElement);

if (module.hot) {
  module.hot.accept('blocks.app/Root', () => {
    const NextRoot = require('blocks.app/Root').default;
    ReactDOM.render(<NextRoot />, rootElement);
  });
}

serviceWorker.unregister();
