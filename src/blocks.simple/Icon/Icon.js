import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cls from 'classnames';
import styles from './Icon.module.scss';

class Icon extends PureComponent {
  static propTypes = {
    type: PropTypes.string,
    size: PropTypes.number,
    children: PropTypes.node,
    className: PropTypes.string,
  };

  static defaultProps = {
    size: 18,
  };

  render() {
    const { type, children, className, size } = this.props;

    if (!children && !type) {
      return null;
    }

    if (children) {
      return <div className={cls(styles.icon, className)}>{children}</div>;
    }

    if (type) {
      const icon = iconsList[type];
      if (!icon) return null;

      return (
        <div className={cls(styles.icon, className, styles[type])}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox={icon.viewBox}
          >
            {icon.content}
          </svg>
        </div>
      );
    }
  }
}

const iconsList = {
  close: {
    content: (
      <path d="M7.719 6.281L6.28 7.72 23.563 25 6.28 42.281 7.72 43.72 25 26.437 42.281 43.72l1.438-1.438L26.437 25 43.72 7.719 42.28 6.28 25 23.563z" />
    ),
    viewBox: '0 0 50 50',
  }
}

export default Icon;
