import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

import cls from 'classnames';
import styles from './Rectangle.module.scss';

class Rectangle extends PureComponent {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    className: PropTypes.string,
    src: PropTypes.string,
    children: PropTypes.node,
  };

  static defaultProps = {
    width: 1,
    height: 1
  };

  render() {
    const {width, height, className, src, children} = this.props;
    const paddingBottom = (height/width)*100;

    return (
      <div className={styles.item}>
        <div className={cls(styles.content, className)}>
          {src &&
          <div
            className={styles.cover}
            style={{backgroundImage: `url(${src})`}}
          >
          </div>
          }
          {children}
        </div>
        <div style={{paddingBottom: `${paddingBottom}%`}} />
      </div>
    );
  }
}

export default Rectangle;
